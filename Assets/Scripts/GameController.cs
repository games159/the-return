using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Player player;

    public TMP_InputField textEntryField;
    public TMP_Text historyText;
    public TMP_Text currentText;

    public Action[] actions;

    [TextArea]
    public string introText;

    private readonly string newLine = System.Environment.NewLine;

    // Start is called before the first frame update
    void Start()
    {
        historyText.text = introText;
        DisplayLocation();
        textEntryField.ActivateInputField();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisplayLocation(bool additive = false)
    {
        var description = player.currentLocation.description + newLine;
        description += player.currentLocation.GetConnectionsText();
        description += player.currentLocation.GetItemsText();
        if (additive)
        {
            currentText.text = currentText.text + "\n" + description;
        }
        else
        {
            currentText.text = description;
        }
    }

    public void TextEntered()
    {
        CurrentTextToHistory();
        ProcessInput(textEntryField.text);
        textEntryField.text = string.Empty;
        textEntryField.ActivateInputField();
    }

    private void CurrentTextToHistory()
    {
        historyText.text += newLine + newLine;
        historyText.text += currentText.text;

        historyText.text += newLine + newLine;
        historyText.text += "<color=#aaccaaff>" + textEntryField.text + "</color>";
    }

    private void ProcessInput(string input)
    {
        input = input.ToLowerInvariant();

        var delimeter = ' ';
        var separatedWords = input.Split(delimeter);

        foreach (var action in actions)
        {
            if (action.keyword.ToLowerInvariant() == separatedWords[0])
            {
                if (separatedWords.Length > 1)
                {
                    action.RespondToInput(this, separatedWords[1]);
                }
                else
                {
                    action.RespondToInput(this, string.Empty);
                }
                
                return;
            }
        }

        currentText.text = "Nothing happens! (having trouble? type Help)";
    }
}
