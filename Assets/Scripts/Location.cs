using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Location : MonoBehaviour
{
    public string locationName;
    [TextArea]
    public string description;
    public Connection[] connections;
    public List<Item> items = new ();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string GetItemsText()
    {
        if (items.Count == 0)
        {
            return string.Empty;
        }

        string result = "You see ";
        var first = true;
        foreach (var item in items)
        {
            if (item.itemEnabled)
            {
                if (!first)
                {
                    result += " and ";
                }

                result += item.description;
                first = false;
            }
        }

        result += System.Environment.NewLine;

        return result;
    }

    public bool HasItem(Item itemToCheck)
    {
        foreach (var item in items)
        {
            if (item == itemToCheck && item.itemEnabled)
            {
                return true;
            }
        }

        return false;
    }

    public string GetConnectionsText()
    {
        var result = string.Empty;

        foreach (var connection in connections)
        {
            if (connection.connectionEnabled)
            {
                result += connection.description + System.Environment.NewLine;
            }
        }

        return result;
    }

    public Connection GetConnection(string connectionNoun)
    {
        foreach (var connection in connections)
        {
            if (connection.connectionName.ToLowerInvariant() == connectionNoun.ToLowerInvariant())
            {
                return connection;
            }
        }

        return null;
    }
}
