using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Actions/Read")]
public class Read : Action
{
    public override void RespondToInput(GameController controller, string noun)
    {
        if (ReadTextOnItem(controller, controller.player.currentLocation.items, noun))
        {
            return;
        }

        if (ReadTextOnItem(controller, controller.player.inventory, noun))
        {
            return;
        }

        controller.currentText.text = "There is no " + noun;
    }

    private bool ReadTextOnItem(GameController controller, List<Item> items, string noun)
    {
        foreach (var item in items)
        {
            if (item.itemName == noun)
            {
                if (controller.player.CanReadItem(controller, item) && item.InteractWith(controller, "read"))
                {
                    return true;
                }

                controller.currentText.text = "There is nothing to read";
                return true;
            }
        }

        return false;
    }
}
