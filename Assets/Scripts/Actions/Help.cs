using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Actions/Help")]
public class Help : Action
{
    public override void RespondToInput(GameController controller, string noun)
    {
        controller.currentText.text = "Type a Verb followed by a noun (e.g. \"go north\")" + System.Environment.NewLine;
        controller.currentText.text += "Allowed verbs:" + System.Environment.NewLine + "Go, Get, Give, Examine, Read, Inventory, Use, TalkTo, Say, Help";
    }
}
